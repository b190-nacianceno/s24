console.log("Hello world!");


// SECTION - Exponent Operator
// es5
/* 
    let/const variableName = Math.pow(number,exponent)
*/
let firstNum = Math.pow(8,2);
console.log(firstNum);

// es6
/* 
let/const variableName = number ** exponent
*/
let secondNum = 8**2;
console.log(secondNum);

// SECTION - Template Literals

/* 
-allows to write strings without using the concatination operator (+);
-helps in code readability
*/

let name = "John";

// pre-ES6
let message = "Hello " + name + "! Welcome to programming!"
// Message without template literals: message
console.log("Message without template literals: " + message);


// ES6
// single-line
message = `hello ${name}! Welcome to programming!`;
console.log(`Message without template literals: ${message}`);

// multiple-line 
const anotherMessage = `
${name} won the math competition.
He wont it by solving the problem 8**2 with the solution of ${secondNum}.
`



console.log(anotherMessage);


/* 
-Template literals allow us to write strings with embedded JS expressions
-expression in general are any valid unit of code that resolves into a value
-${} are used to include JS expressions in strings using template literals
*/
let interestRate = .15;
principal = 1000;

console.log(`The remaining balance of your account is ${principal}. You earned ${principal*interestRate} in interest `);

// SECTION -Array Destructuring

/* 
-allows us to unpack elements in arrays to distinct variables
-allows us to name array elements with variables instead of using index numbers
-helps with code readability 
SYNTAX:
    let/const[variableA, variableB, variableC] = arrayName

*/

const fullName = ['Juan', 'Dela', 'Cruz'];
// pre-array destructuring
console.log(fullName[0]);
console.log(fullName[1]);
console.log(fullName[2]);

// array destructuring
// const [firstName, middleName, lastName] = fullName;
// console.log(firstName);
// console.log(middleName);
// console.log(lastName);

// // using template literals

// console.log(`Hello ${firstName} ${middleName} ${lastName}!`);


// SECTION - Object Desctructuring
/* 
-allows us to unpack properties of objects into distinct variables
-shortens the syntax for accessing the properties from an object

SYNTAX:
    let/const {propoertyA, propertyB, propertyC} = objectName;
Note: will reutrn an error if the dev did not use the correct propertyName
*/

const person = {
    firstName: "Jane",
    middleName: "Dela",
    lastName: "Cruz"
}

// pre-es6 or pre-object destructuring

console.log(person.firstName);
console.log(person.middleName);
console.log(person.lastName);
console.log(`Hello ${person.firstName} ${person.middleName} ${person.lastName}`);

// object destructuring
const {firstName,middleName,lastName} = person
console.log(firstName);
console.log(middleName);
console.log(lastName);
console.log(`Hello ${firstName} ${middleName} ${lastName}!`);


// using object destructuring as a parameter of a function
function getFullName({firstName, middleName, lastName}){
    console.log(`${firstName} ${middleName} ${lastName}`);
};
getFullName(person);

const pet = {
    petName: "June",
    trick: "fist bump",
    treat: "bone"
};

// let {petName,trick,treat} = pet

function callpet({petName,trick,treat}){
    console.log(`${petName}, ${trick}!`);
    console.log(`Good, ${trick}!`);
    console.log(`Here is the, ${treat} for you!`);

};

callpet(pet);

// SECTION Arrow Function
/* 
-compact alternatie syntax to traditional functions
*/


// const hello = ()=>{
//     console.log("Hello World");
// };

// hello();

// pre-e6 arrow function

// function printFullName (firstName,middleInitial,lastName){
//     console.log(`${firstName} ${middleInitial} ${lastName}`)
// };

// ES-6 arrow function
/* 
let/const variableName = (parameterA, parameterB, parameterC) => {
    console.log();
    statements/expression
}
*/

const printFullName = (firstName,middleInitial, lastName) => {
    console.log(`${firstName} ${middleInitial} ${lastName}`)
};


printFullName("Portgas", "D.", "Ace");

// Note on norms:
// arrow functions for short term functions
// uses traditional syntax for more used functions throughout the document

// Arrow function with loops
// pre-arrow function

const students = ["John",'Jane',"Judy"];
students.forEach(function(student){
    console.log(`${student} is a student.`);
});


// let add = (x,y) => {
//     return x+y;
// }

// console.log(add(5,4));

// arrow function
const add = (x,y) => x+y;
console.log(add(5,8));

// SECTION - Default Argument Value

/* 
the "name = 'User" sets the default value for function greet() once it is called withouth any parameters
*/
const greet = (name = "User", age = "unknown") => {
    return`Good morning ${name}!`
}

console.log(greet("John"));
// this would return "Good morning User" because the function is called without any arguments
console.log(greet());

// SECTION - Class-based Object Blue Prints

/* 
-allows creation/instantiation of objects using classes as blueprints
*/

/* 
Creating a class 
    -"constructor" is a special method of a class for creating/initializing an object for that clas
    -"this" keyword refers to the properties of an object created from the class
    -allows us to reassign values for the properties inside the class
    SYNTAX:
        class Classname {
            constructor (property1, property2,property3){
                this.property1 = property1;
                this.property2 = property2;
                this.property3 = property3;
            }
        }
*/

class Car {
    constructor (brand,name,year){
        this.brand = brand;
        this.name = name;
        this.year = year;

    }
};

// first instance
// using initializer/dot notation
/* 
-"new" creates a new object with the given arguments as the values of its properties
-no arguments provided will create an object without any values assigned to ti, meaning the properties would return "undefined"
SYNTAX:
    let/const variableName = new ClassName();
*/


const myCar = new Car ();
myCar.brand = "Jeep";
myCar.name = "Wrangler";
myCar.year = "2000";

console.log(myCar);

// second instance using arguments

const myNewCar = new Car ("Toyota", "Vios",2021);
console.log(myNewCar);
